﻿using CapsellaStore.Services.Interfaces.DBContext;
using Microsoft.EntityFrameworkCore;

namespace CapsellaStore.Infrastructure.Data.MSSQL
{
    public class AppMSSQLContext : AppDBContext
    {
        private const string _connectionString = "Server=(localdb)\\mssqllocaldb;Database=CapsellaStoreData;Trusted_Connection=True;";

        public AppMSSQLContext(DbContextOptions<AppMSSQLContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            optionsBuilder.UseSqlServer(_connectionString);

            base.OnConfiguring(optionsBuilder);
        }
    }
}
