﻿using System;

namespace CapsellaStore.Domain.Core.Base
{
    /// <summary>
    /// Доменный класс
    /// </summary>
    public class DomainObject : IEquatable<DomainObject>
    {
        /// <summary>
        /// Индификатор
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Поверхностное клонирование
        /// </summary>
        /// <returns>DomainObject</returns>
        public DomainObject ShallowClone() => this.MemberwiseClone() as DomainObject;

        /// <summary>
        /// Глубокое клонирование
        /// </summary>
        /// <returns>DomainObject</returns>
        public virtual DomainObject DeepClone() => ShallowClone();

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            return Equals(obj as DomainObject);
        }

        public override int GetHashCode() => base.GetHashCode();

        public bool Equals(DomainObject other)
        {
            if (GetType() != other.GetType()) return false;
            return Id == other.Id;
        }
    }
}