﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CapsellaStore.Domain.Core
{
    /// <summary>
    /// Пользователь
    /// </summary>
    public class AppUser : IdentityUser
    {
    }
}
