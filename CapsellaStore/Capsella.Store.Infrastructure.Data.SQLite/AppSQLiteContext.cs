﻿using CapsellaStore.Services.Interfaces.DBContext;
using Microsoft.EntityFrameworkCore;

namespace CapsellaStore.Infrastructure.Data.SQLite
{
    public class AppSQLiteContext : AppDBContext
    {
        private const string _connectionString = "Filename=CapsellaStoreData.db";

        public AppSQLiteContext(DbContextOptions<AppSQLiteContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            
            optionsBuilder.UseSqlite(_connectionString);

            base.OnConfiguring(optionsBuilder);
        }
    }
}
