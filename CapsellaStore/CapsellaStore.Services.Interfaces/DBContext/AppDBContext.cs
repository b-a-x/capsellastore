﻿using CapsellaStore.Domain.Core;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace CapsellaStore.Services.Interfaces.DBContext
{
    /// <summary>
    /// общий класс контекста для доступа к данным
    /// </summary>
    public abstract class AppDBContext : IdentityDbContext<AppUser>
    {
        protected AppDBContext()
        {
        }

        public AppDBContext(DbContextOptions options) : base(options)
        {
        }
    }
}
