﻿using CapsellaStore.Infrastructure.Data.MSSQL;
using CapsellaStore.Infrastructure.Data.SQLite;
using Microsoft.Extensions.DependencyInjection;

namespace CapsellaStore.Infrastructure.WebApp.Extensions.DataBaseContext
{
    /// <summary>
    /// Класс для настройки контекста базы данных
    /// </summary>
    public static class DataBaseContext
    {
        public static void AddAppDBContext(this IServiceCollection services)
        {
            services.AddDbContext<AppSQLiteContext>();
        }
    }
}
