﻿using CapsellaStore.Domain.Core;
using CapsellaStore.Infrastructure.Data.MSSQL;
using CapsellaStore.Infrastructure.Data.SQLite;
using CapsellaStore.Infrastructure.WebApp.Authorize;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace CapsellaStore.Infrastructure.WebApp.Extensions.Identity
{
    /// <summary>
    /// Класс для настройки авторизации и аунтификации
    /// </summary>
    public static class DefaultIdentity
    {
        public static void AddAppDefaultIdentity(this IServiceCollection services)
        {
            services.AddDefaultIdentity<AppUser>(options => options.SignIn.RequireConfirmedAccount = true)
                .AddUserManager<AppUserManager>()
                .AddSignInManager<AppSignInManager>()
                .AddEntityFrameworkStores<AppSQLiteContext>();
        }
    }
}
