﻿using CapsellaStore.Domain.Core;
using CapsellaStore.Infrastructure.WebApp.Authorize;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace CapsellaStore.Infrastructure.WebApp.Extensions.Inject
{
    /// <summary>
    /// Класс для добавления зависимостей
    /// </summary>
    public static class InjectExtensions
    {
        public static void AddAppInjection(this IServiceCollection services)
        {
            AddTransient(services);
            AddScoped(services);
            AddSingleton(services);
        }

        private static void AddTransient(IServiceCollection services)
        {
        }

        private static void AddScoped(IServiceCollection services)
        {
            services.AddScoped<UserManager<AppUser>, AppUserManager>();
            services.AddScoped<SignInManager<AppUser>, AppSignInManager>();
        }

        private static void AddSingleton(IServiceCollection services)
        {
        }
    }
}
